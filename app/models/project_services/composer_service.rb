# == Schema Information
#
# Table name: services
#
#  id                    :integer          not null, primary key
#  type                  :string(255)
#  title                 :string(255)
#  project_id            :integer
#  created_at            :datetime
#  updated_at            :datetime
#  active                :boolean          default(FALSE), not null
#  properties            :text
#  template              :boolean          default(FALSE)
#  push_events           :boolean          default(TRUE)
#  issues_events         :boolean          default(TRUE)
#  merge_requests_events :boolean          default(TRUE)
#  tag_push_events       :boolean          default(TRUE)
#  note_events           :boolean          default(TRUE), not null
#

class ComposerService < Service
  prop_accessor :export_branches, :restrict_to_branch, :export_tags, :restrict_to_tag

  after_save :process_packages

  def title
    'Composer'
  end

  def description
    'This project will be publicly listed as a composer package, but usage of private and internal repositories will still require authentication.'
  end

  def help
    'The package is exported using the project\'s composer.json'
  end

  def to_param
    'composer'
  end

  def fields
    [
        {
            type: 'checkbox',
            name: 'export_branches'
        },
        {
            type: 'text',
            name: 'restrict_to_branch',
            placeholder: 'Comma-separated list of branches which will be automatically inspected. Leave blank to include all branches.'
        },
        {
            type: 'checkbox',
            name: 'export_tags'
        },
        {
            type: 'text',
            name: 'restrict_to_tag',
            placeholder: 'Comma-separated list of tags which will be automatically inspected. Leave blank to include all tags.'
        }
    ]
  end

  def supported_events
    %w(push tag_push)
  end

  # disable test button
  def can_test?
    false
  end

  def update_attributes(attributes)
    @changed_properties = properties.dup
    super
  end

  def changed_properties
    @changed_properties ||= ActiveSupport::HashWithIndifferentAccess.new
  end

  # Provide convenient accessor methods
  # for each serialized property.
  def self.prop_accessor(*args)
    args.each do |arg|
      class_eval %{
        def #{arg}
          properties['#{arg}']
        end

        def #{arg}=(value)
          self.properties['#{arg}'] = value
        end

        def #{arg}_was
          changed_properties.present? && changed_properties['#{arg}'] != properties['#{arg}'] ? changed_properties['#{arg}'] : properties['#{arg}']
        end
      }
    end
  end

  def process_packages
    # process packages for all tags
    project.repository.tags.each do |tag|
      process_commit(tag)
    end

    # process packages for all branches
    project.repository.branches.each do |branch|
      process_commit(branch)
    end
  end

  def process_commit(ref)
    removed = remove_previous_package(ref)
    begin

      package = Composer::Package.new(project, ref)

      if activated? && export_commit?(ref)
        manager.add_package(package)
      elsif !removed
        manager.rm_package(package)
      end

    rescue
      # skip on error
    end
  end

  def remove_previous_package(ref)
    removed = false
    begin

      if commit_was_activated? && export_commit?(ref)

        package = Composer::Package.new(project, ref)

        manager.rm_package(package)
        removed = true

      end

    rescue
      # skip on error
    end
    removed
  end

  def execute(data)
    after = data[:after]
    ref = Gitlab::Git.ref_name(data[:ref])

    # sync our changes
    if Gitlab::Git.blank_ref?(after) # removing branch

      # recreate exported packages since we dont have access to the original push
      manager.clear_packages

      process_packages

    else # push create / modify
      array Gitlab::Git.tag_ref?(data[:ref]) ? project.repository.tags : project.repository.branches
      match = array.detect do |i|
        i.name == ref && i.target == after
      end
      process_commit(match) unless !match
    end

  rescue
    # skip on error
  end

  private

  def manager
    @manager ||= Composer::Manager.new(project)
  end

  def export_commit?(ref)
    if ref.instance_of?(Gitlab::Git::Branch)
      export_branch?(ref)
    elsif ref.instance_of?(Gitlab::Git::Tag)
      export_tag?(ref)
    else
      false
    end
  end

  def export_branch?(branch)
    branch_restriction = restrict_to_branch.to_s

    export_branches && (branch_restriction.length == 0 || !branch_restriction.index(branch).nil?)
  end

  def export_tag?(tag)
    tag_restriction = restrict_to_tag.to_s

    export_tags && (tag_restriction.length == 0 || !tag_restriction.index(tag).nil?)
  end

  def commit_was_activated?
    active_was && !activated?
  end

  def commit_was_exported?(ref)
    if ref.instance_of?(Gitlab::Git::Branch)
      branch_was_exported?(ref)
    elsif ref.instance_of?(Gitlab::Git::Tag)
      tag_was_exported?(ref)
    else
      false
    end
  end

  def branch_was_exported?(branch)
    branch_restriction = restrict_to_branch_was.to_s

    export_branches_was && (branch_restriction.length == 0 || !branch_restriction.index(branch).nil?)
  end

  def tag_was_exported?(tag)
    tag_restriction = restrict_to_tag_was.to_s

    export_tags_was && (tag_restriction.length == 0 || !tag_restriction.index(tag).nil?)
  end

end
